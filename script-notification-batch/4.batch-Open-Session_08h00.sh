#!/bin/bash
#---HuyBui---#
NAME="Batch Send Open Trading Session"

STA="Start OPEN_TRADING_SESSION command at"
SUCC="Sent OPEN_TRADING_SESSION command successfully at"
ERR="FAILED send OPEN_TRADING_SESSION command"

C_STA=`cat /var/log/pix-batch/pix-batch.log | grep "$STA"`
C_SUCC=`cat /var/log/pix-batch/pix-batch.log | grep "$SUCC"`
C_ERR=`cat /var/log/pix-batch/pix-batch.log | grep "$ERR"`

#if [[ $C_STA == *$STA* && $C_SUCC == *$SUCC* ]]; then
#    echo "Dear all, OM team report `date +%d/%m/%Y`"
#    echo "- $NAME: run SUCCESSFULL."
#    echo "Kindly be informed!"
#elif [[ $C_ERR == *$ERR* ]]; then
#    echo "Dear all, OM team report `date +%d/%m/%Y`"
#    echo "- $NAME: Run FAIL."
#    echo "Kindly be informed!"
#fi

echo "Subject: Notification check RUN Batch PROD Enviroment `date +%F`
From: OMTDT@usasiahrs.poem2.com
`date`" > /opt/script/logs_batch/0.Batch.txt

if [[ $C_STA == *$STA* && $C_SUCC == *$SUCC* ]]; then
    echo "Dear all, OM team report `date +%d/%m/%Y` 
- $NAME: run SUCCESSFULLY.
Kindly be informed!" >> /opt/script/logs_batch/0.Batch.txt
elif [[ $C_ERR == *$ERR* ]]; then
    echo "Dear all, OM team report `date +%d/%m/%Y`
- $NAME: Run FAIL.
Kindly be informed!" >> /opt/script/logs_batch/0.Batch.txt
else
#        echo " $NAME: Not Start." >> 0.Batch.txt

        echo "Dear all, OM team report `date +%d/%m/%Y`
- $NAME: Not Running.
Kindly be informed!" >> /opt/script/logs_batch/0.Batch.txt
fi

/usr/sbin/sendmail thangnguyen3@tdt.asia < /opt/script/logs_batch/0.Batch.txt
/usr/sbin/sendmail trongpham@tdt.asia < /opt/script/logs_batch/0.Batch.txt
/usr/sbin/sendmail tungnguyen1@tdt.asia < /opt/script/logs_batch/0.Batch.txt
/usr/sbin/sendmail manhnguyen1@tdt.asia < /opt/script/logs_batch/0.Batch.txt
/usr/sbin/sendmail huybui1@tdt.asia < /opt/script/logs_batch/0.Batch.txt
/usr/sbin/sendmail nguyennguyen1@tdt.asia < /opt/script/logs_batch/0.Batch.txt
