#!/bin/bash
tag=`date "+%Y%m%d_%H%M"`
tag_current=`docker ps --format {{.Image}} | grep $1 | awk -F':' '{print $2}'`
sed "s|TAG_CURRENT|$tag_current|g" rollback-tmp.sh > rollback.sh
sed "s|TAGIMAGE|$tag|g" docker-compose-tmp.yaml > docker-compose.yaml
cd /opt/poem/poem-platform && docker-compose --compatibility up -d --build $1

sh /opt/script/6_Scan_image.sh $1 $tag
