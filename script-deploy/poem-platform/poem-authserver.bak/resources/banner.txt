${AnsiColor.BRIGHT_BLUE}   ____     _____   ______                          _    _   _______   _    _    _____   ______   _____   __      __  ______   _____
${AnsiColor.BRIGHT_BLUE}  / __ \   / ____| |  ____|                 /\     | |  | | |__   __| | |  | |  / ____| |  ____| |  __ \  \ \    / / |  ____| |  __ \
${AnsiColor.BRIGHT_BLUE} |    | | | (___   | |__      ______       /  \    | |  | |    | |    | |__| | | (___   | |__    | |__) |  \ \  / /  | |__    | |__) |
${AnsiColor.BRIGHT_BLUE} | |  | |  \___ \  |  __|    |______|     / /\ \   | |  | |    | |    |  __  |  \___ \  |  __|   |  _  /    \ \/ /   |  __|   |  _  /
${AnsiColor.BRIGHT_BLUE} | |__| |  ____) | | |____               / ____ \  | |__| |    | |    | |  | |  ____) | | |____  | | \ \     \  /    | |____  | | \ \
${AnsiColor.BRIGHT_BLUE}  \___ /  |_____/  |______|             /_/    \_\  \____/     |_|    |_|  |_| |_____/  |______| |_|  \_\     \/     |______| |_|  \_\
${AnsiColor.BRIGHT_BLUE}
${AnsiColor.BRIGHT_WHITE} OSE - AuthServer - Running Spring Boot ${spring-boot.version}