#!/bin/bash
tag=TAG_CURRENT
sed "s|TAGIMAGE|$tag|g" docker-compose-tmp.yaml > docker-compose.yaml
cd /opt/poem/poem-platform && docker-compose --compatibility up -d $1
