#!/bin/bash
# Script run API Pi-X
# How to use:
# ./start_pix_ws.sh >> /var/log/pix-ws/pix-ws.log

# Setting here
CURRENT_DATE=`date --date "-1 days" +"%Y-%m-%d"`

PIX_REPOSITORY="/opt/pix-ws"

echo "$(date +"%Y-%m-%d %T:%Z") Run by: $(id -u -n)"

runAPI() {
    echo "Start run API Pi-X."
    #kill -9 $(lsof -t -i:8769)
    cd ${PIX_REPOSITORY}
    nohup java -jar pix-ws.jar --spring.config.location=${PIX_REPOSITORY}/resources/,${PIX_REPOSITORY}/resources/config/ >/dev/null 2>&1 &
    return_code=$?
    echo "Finish run API Pi-X."
    return $return_code
}

if runAPI; then
    echo "Run API ${CURRENT_DATE} success."
else
    echo "Run API ${CURRENT_DATE} failed."
fi
