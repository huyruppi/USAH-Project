#!/bin/bash
tag=`date "+%Y%m%d_%H%M"`
tag_current=`docker ps --format {{.Image}} | grep poem-ws | awk -F':' '{print $2}'`
sed "s|TAG_CURRENT|$tag_current|g" rollback-ws-tmp.sh > rollback-ws.sh
sed "s|TAGIMAGE|$tag|g" docker-compose-tmp.yaml > docker-compose.yml
#mv /opt/poem/poem-retail/config.js /opt/poem/poem-retail/build/js/
docker-compose up -d --build

sh /opt/script/6_Scan_image.sh poem-ws $tag

