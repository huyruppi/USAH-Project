window.globalThis = {
    "wsUrl": "wss://pixstg.tdt.asia/ws/api/v1/websocket",
    "apiUrl": "https://pixstg.tdt.asia/api",
    "flagRmsApi": "true",
    "idleTimeOut": 3600000,
    "countDownTime": 17000,
    "timesChangePassword": 6
}
