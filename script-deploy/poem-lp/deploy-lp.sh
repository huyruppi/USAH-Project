#!/bin/bash
rm -rf build-lp
unzip build-lp.zip
tag=`date "+%Y%m%d_%H%M"`
tag_current=`docker ps --format {{.Image}} | grep poem-lp | awk -F':' '{print $2}'`
rm build-lp/js/config.js
cp config.js build-lp/js/config.js
sed "s|TAG_CURRENT|$tag_current|g" rollback-lp-tmp.sh > rollback-lp.sh
sed "s|TAGIMAGE|$tag|g" docker-compose-tmp.yaml > docker-compose.yaml
docker-compose up -d --build
sh /opt/script/6_Scan_image.sh poem-lp $tag
