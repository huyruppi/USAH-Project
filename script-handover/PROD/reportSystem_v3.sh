#!/bin/bash
# Report All System

ipa=("10.148.72.99" "10.148.73.99")
ipw=( "10.148.72.68" "10.148.73.68")
sv_worker=("poem-lp" "poem-retail" "poem-remisier" "poem-authserver" "poem-gateway" "poem-marketservice" "poem-registry" "poem-ws")
sv_backend=("poem-backend" "poem-nginx-backend" "elk_elasticsearch" "elk_kibana" "elk_logstash" "elk_filebeat")

DATE=`date +%Y%m%d`
time=`date +%H`
DIR=/opt/script/notifications/handover

rm -rf $DIR/list_healthy.txt

echo "Subject: All status of Server and Service PROD Environment.
From: OMTDT@usasiahrs.poem2.com"
echo "`date`"
echo -e "\nDear all, \nOM team report all status of Server and Service PROD Environment."
echo -e "\n---------------------REPORT STATUS ALL Server---------------------"

#Check Server
         for ip in ${ipa[@]}
                do
                        host_name=`ssh poem@${ip} hostname`
                        if [ "ip-10-148-72-99.ap-southeast-1.compute.internal" == $host_name ]; then
                                hostname="PROD_BACKEND:"
                        else
                                hostname="PROD_CORE:"
                        fi
                        DISK_USE=`ssh poem@${ip} df -h / | grep dev | awk '{printf "%.2f%%", $5}'`
                        MEMORY=`ssh poem@${ip} free -m | awk 'NR==2{printf "%.2f%%", $3*100/$2 }'`
                        CPU=`ssh poem@${ip} cat /proc/loadavg | awk '{print $1, $2, $3}'`
                        echo -e "- "$hostname"\t D: "$DISK_USE"\t M: "$MEMORY"\t C: "$CPU""
                        echo -e "$DISK_USE\n$MEMORY\n$CPU" >> $DIR/list_healthy.txt
        done
        for ip in ${ipw[@]}
                do
                        host_name=`ssh poem@${ip} hostname`
                        if [ "ip-10-148-72-68.ap-southeast-1.compute.internal" == $host_name ]; then
                                hostname="PROD_WORKER_A:"
                        else
                                hostname="PROD_WORKER_B:"
                        fi
                        DISK_USE=`ssh poem@${ip} df -h / | grep dev | awk '{printf "%.2f%%", $5}'`
                        MEMORY=`ssh poem@${ip} free -m | awk 'NR==2{printf "%.2f%%\t\t", $3*100/$2 }'`
                        CPU=`ssh poem@${ip} cat /proc/loadavg | awk '{print $1, $2, $3}'`
                        echo -e "- "$hostname" D: "$DISK_USE"\t M: "$MEMORY"\t C: "$CPU""
                        echo -e "$DISK_USE\n$MEMORY\n$CPU" >> $DIR/list_healthy.txt
        done
# Check PID, PID_NAME Service
        echo -e "\n-----------------REPORT STATUS ALL Service------------------"

        maintrade=`ssh poem@10.148.73.99 pgrep -f matching-gw-0.0.1.jar`
        grok_time=`ssh poem@10.148.73.99 systemctl status poem-core | grep "Active" | awk -F';' '{print $2}'`
        echo -e "- "PID_Core:"\t "$maintrade" ----------Time: Up$grok_time"
        echo -e "$maintrade\n$grok_time" >> $DIR/list_healthy.txt

        pid_batch=`ssh poem@10.148.72.99 pgrep -f batch-0.0.1-SNAPSHOT.jar`
        time_batch=`ssh poem@10.148.72.99 systemctl status poem-batch | grep "Active" | awk -F';' '{print $2}'`
        echo -e "- "PID_Batch:"\t "$pid_batch" -----------Time: Up$time_batch"
        echo -e "$pid_batch\n$time_batch" >> $DIR/list_healthy.txt

#Check Backend Service
        for cont in ${sv_backend[@]}
		do      
                        command="docker ps --filter=name=$cont --format '- CID_{{.Names}}: {{.ID}} --- Time: {{.Status}}'"
                        command1="docker ps --filter=name=$cont --format '{{.ID}}\n{{.Status}}'"
                        ssh poem@10.148.72.99 $command
                        ssh poem@10.148.72.99 $command1 >> $DIR/list_healthy.txt
	        done

#Check Service Worker-A,B
 for ip in ${ipw[@]}
                do
                        host_name=`ssh poem@${ip} hostname`
                        if [ "ip-10-148-72-68.ap-southeast-1.compute.internal" == $host_name ]; then
                                workername="Worker-A"
                        else
                                workername="Worker-B"
                        fi
                        echo -e "\n--------------REPORT STATUS container service $workername ------------------"
                        for cont in ${sv_worker[@]}
		        do      
                                command="docker ps --filter=name=$cont --format '- CID_{{.Names}}: {{.ID}} --- Time: {{.Status}}'"
                                command1="docker ps --filter=name=$cont --format '{{.ID}}'"
                                ssh poem@$ip $command
                                ssh poem@$ip $command1 >> $DIR/list_healthy.txt
	                done	       
        done
echo -e "\n------------------------ALL REPORT SYSTEM: DONE-----------------------"
echo -e "\nKindly be informed!"

if [ $time == 12 ]
	then
		sed -i 's|AF|MO|' $DIR/USA_CreateHandover.py
	else
		sed -i 's|MO|AF|' $DIR/USA_CreateHandover.py
	fi
	sleep 1

/root/.pyenv/versions/handover-env/bin/python /opt/script/notifications/handover/USA_CreateHandover.py	

rm -rf $DIR/list_healthy.txt
mkdir -p /home/ec2-user/handover/backup/$DATE
cp -r /home/ec2-user/handover/*.xlsx  /home/ec2-user/handover/backup/$DATE
chown -R ec2-user. /home/ec2-user/handover/*
