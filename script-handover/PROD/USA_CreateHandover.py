import numpy as np

from openpyxl import load_workbook
from datetime import date, timedelta

today = date.today()
date = today.strftime("%Y/%m/%d")

my_file = open("/opt/script/notifications/handover/list_healthy.txt", "r")
data = my_file.read()
data = data.split('\n')
my_file.close()

wb = load_workbook('/opt/script/notifications/handover/Form_AF_USA_PROD_HandOver_SYSTEM.xlsx')
sheets = wb.active

sheets["D2"] = date

d = 0
for i in range (11, 15):
    for j in range (3, 6):
        sheets.cell(row = i, column = j, value = data[d])
        d+=1
d = 12
for i in range (18, 20):
    for j in range (3, 5):
        sheets.cell(row = i, column = j, value = data[d])
        d+=1

d = 16
for i in range (22, 28):
    for j in range (3, 5):
        sheets.cell(row = i, column = j, value = data[d])
        d+=1

d = 28
for i in range (3, 5):
    for j in range (30, 38):
        sheets.cell(row = j, column = i, value = data[d])
        d+=1

wb.save('/home/ec2-user/handover/AF_USA_PROD_CheckList_HandOver_SYSTEM.xlsx')



