#!/bin/bash
################
# Report All System
echo "DAY `date +%d-%m-%Y`"
echo -e "Dear all, \nOM team report all status of Server and Service STG Environment."
echo -e "\n---------------------REPORT STATUS ALL Server---------------------"
pix=`cat /opt/script/notifications/.pass`
worker="Xy2T5XdUWC8gVzG3od"
ipa=("10.3.14.159" "10.3.15.14")
ipw=( "10.3.14.4" "10.3.15.39")
container=("poem-lp" "poem-retail" "poem-ws" "poem-authserver" "poem-gateway" "poem-marketservice" "poem-registry")

#Check Server
         for ip in ${ipa[@]}
                do
                        host_name=`sshpass -p "$pix" ssh poem@${ip} hostname`
                        if [ "ip-10-3-14-159.ap-southeast-1.compute.internal" == $host_name ]; then
                                hostname="STG_BACKEND:"
                        else
                                hostname="STG_CORE:"
                        fi
                        DISK_USE=`sshpass -p "$pix" ssh poem@${ip} df -h / | grep -vi 'avail|mapper' | grep dev | awk '{print $5}'`
                        MEMORY=`sshpass -p "$pix" ssh poem@${ip} free -m | awk 'NR==2{printf "%.2f%%\t\t", $3*100/$2 }'`
                        CPU=`sshpass -p "$pix" ssh poem@${ip} cat /proc/loadavg | awk '{print $1, $2, $3}'`
                        echo -e "- "$hostname"\t D: "$DISK_USE"\t M: "$MEMORY"\t C: "$CPU""
        done
        for ip in ${ipw[@]}
                do
			host_name_work=`sshpass -p "$worker" ssh poem@${ip} hostname`
                        if [ "ip-10-3-14-4.ap-southeast-1.compute.internal" == $host_name_work ]; then
                                hostname="STG_WORKER_1:"
                        else
                                hostname="STG_WORKER_2:"
                        fi
                        DISK_USE=`sshpass -p "$worker" ssh poem@${ip} df -h / | grep -vi 'avail|mapper' | grep dev | awk '{print $5}'`
                        MEMORY=`sshpass -p "$worker" ssh poem@${ip} free -m | awk 'NR==2{printf "%.2f%%\t\t", $3*100/$2 }'`
                        CPU=`sshpass -p "$worker" ssh poem@${ip} cat /proc/loadavg | awk '{print $1, $2, $3}'`
                        echo -e "- "$hostname"\t D: "$DISK_USE"\t M: "$MEMORY"\t C: "$CPU""
        done
# Check PID, PID_NAME Service
        echo -e "\n-----------------REPORT STATUS ALL Service------------------"

        maintrade=`sshpass -p "$pix" ssh poem@10.3.15.14 pgrep -f matching-gw-0.0.1.jar`
#	time_maintrade=`systemctl status poem-core | grep "Active" | awk -F';' '{print $2}'`
	grok_time=`sshpass -p "$pix" ssh poem@10.3.15.14 systemctl status poem-core | grep "Active" | awk -F';' '{print $2}'`
        echo -e "- "PID_Core:"\t "$maintrade" ----------UPTIME: $grok_time"

        pid_batch=`pgrep -f batch-0.0.1-SNAPSHOT.jar`
	time_batch=`sudo systemctl status poem-batch | grep "Active" | awk -F';' '{print $2}'`
        echo -e "- "PID_Batch:"\t "$pid_batch" -----------UPTIME: $time_batch"

        pid_backend=`sudo systemctl status backend | grep "Main PID" | awk '{print $3}'`
	time_backend=`sudo systemctl status backend | grep "Active" | awk -F';' '{print $2}'`
	echo -e "- "PID_Backend:"\t "$pid_backend" ---------UPTIME: $time_backend"

#Check Container ID Worker-1
	echo -e "\n--------------REPORT STATUS ALL Container Worker-1------------------"
	for cont in ${container[@]}
		do 
			container_id=`sshpass -p "$worker" ssh poem@10.3.14.4 docker ps | grep $cont | awk '{print $1}'`             
			if [ "poem-lp" == $cont -o "poem-retail" == $cont ]; then
                                time_live=`sshpass -p "$worker" ssh poem@10.3.14.4 docker ps | grep $cont | awk '{print $8, $9}'`
                        else
                                time_live=`sshpass -p "$worker" ssh poem@10.3.14.4 docker ps | grep $cont | awk '{print $10, $11}'`
                        fi
			echo -e "- "CID_${cont}: $container_id ------ "UPTIME: $time_live"

	done	
#Check Container ID Worker-2
        echo -e "\n--------------REPORT STATUS ALL Container Worker-2------------------"
        for cont in ${container[@]}
                do
                        container_id=`sshpass -p "$worker" ssh poem@10.3.15.39 docker ps | grep $cont | awk '{print $1}'`
			if [ "poem-lp" == $cont -o "poem-retail" == $cont ]; then
                                time_live=`sshpass -p "$worker" ssh poem@10.3.15.39 docker ps | grep $cont | awk '{print $8, $9}'`
                        else
                                time_live=`sshpass -p "$worker" ssh poem@10.3.15.39 docker ps | grep $cont | awk '{print $10, $11}'`
                        fi
        echo -e "- "CID_${cont}: $container_id --------- "UPTIME: $time_live"
        done

echo -e "\n------------------------ALL REPORT SYSTEM: DONE-----------------------"
echo -e "\nKindly be informed!"


