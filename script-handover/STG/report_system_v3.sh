#!/bin/bash
# Report All System

ipa=("10.3.14.159" "10.3.15.14")
ipw=( "10.3.14.4" "10.3.15.39")
sv_worker=("poem-lp" "poem-retail" "poem-remisier" "poem-authserver" "poem-gateway" "poem-marketservice" "poem-registry" "poem-ws")
sv_backend=("poem-backend" "poem-nginx-backend" "ELK_Elasticsearch" "ELK_Kibana" "ELK_Logstash" "ELK_Filebeat")

rm -rf list_healthy.txt

#Check Status Server
         for ip in ${ipa[@]}
                do
                        DISK_USE=`ssh poem@${ip} df -h / | grep dev | awk '{print $5}'`
                        MEMORY=`ssh poem@${ip} free -m | awk 'NR==2{printf "%.2f%%\t\t", $3*100/$2 }'`
                        CPU=`ssh poem@${ip} cat /proc/loadavg | awk '{print $1, $2, $3}'`
                        echo -e "$DISK_USE\n$MEMORY\n$CPU" >> list_healthy.txt
        done
        for ip in ${ipw[@]}
                do
                        DISK_USE=`ssh poem@${ip} df -h / | grep dev | awk '{print $5}'`
                        MEMORY=`ssh poem@${ip} free -m | awk 'NR==2{printf "%.2f%%\t\t", $3*100/$2 }'`
                        CPU=`ssh poem@${ip} cat /proc/loadavg | awk '{print $1, $2, $3}'`
                        echo -e "$DISK_USE\n$MEMORY\n$CPU" >> list_healthy.txt
        done
# Check Batch, Core Service
        maintrade=`ssh poem@10.3.15.14 pgrep -f matching-gw-0.0.1.jar`
        grok_time=`ssh poem@10.3.15.14 systemctl status poem-core | grep "Active" | awk -F';' '{print $2}'`
        echo -e "$maintrade\n$grok_time" >> list_healthy.txt

        pid_batch=`pgrep -f batch-0.0.1-SNAPSHOT.jar`
        time_batch=`systemctl status poem-batch | grep "Active" | awk -F';' '{print $2}'`
        echo -e "$pid_batch\n$time_batch" >> list_healthy.txt

#Check Backend Service
        for cont in ${sv_backend[@]}
		do      
                        command="docker ps --filter=name=$cont --format '{{.Names}}\n{{.ID}}\n{{.Status}}'"
                        ssh poem@10.3.14.159 $command >> list_healthy.txt
	        done

#Check Service Worker-A
	for cont in ${sv_worker[@]}
		do      
                        command="docker ps --filter=name=$cont --format '{{.Names}}\n{{.ID}}'"
                        ssh poem@10.3.14.4 $command >> list_healthy.txt
	        done	

#Check Service Worker-B
        for cont in ${sv_worker[@]}
		do 
                        command="docker ps --filter=name=$cont --format '{{.ID}}'"
                        ssh poem@10.3.15.39 $command >> list_healthy.txt
                done
echo "------" >> list_healthy.txt



