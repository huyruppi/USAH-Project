#!/bin/bash
cd /opt/backup/image-backup
for i in `docker images | awk -F' ' '{print $1":"$2}'`
do
	docker save -o $i.tar $i
        zip -r $i.tar.zip $i.tar
	rm -rf $i.tar	
	echo "backup $i image done"
done
