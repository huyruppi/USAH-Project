#!/bin/bash

DATE=`date +%Y-%m-%d`
BACKUP_DIR=/opt/backup/database_backups
cp ${BACKUP_DIR}/daily_backup/poemsquare_prod.${DATE}.zip ${BACKUP_DIR}/month_backup/

for i in `cd ${BACKUP_DIR}/month_backup && ls -1tr | head -n -12`;do
	aws s3 rm s3://usasiahours-backup-logs/database_backups/month_backup/$i
done
cd ${BACKUP_DIR}/month_backup/ && ls -1tr | head -n -12 | xargs -d '\n' rm -f --
