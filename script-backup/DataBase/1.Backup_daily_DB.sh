#!/bin/bash

# vars
BACKUP_DIR=/opt/backup/database_backups/daily_backup
ODOO_DATABASE=poemsquare_prod
ADMIN_PASSWORD=o8zzsYQ3RxJvAd5v3eTz

# create a backup directory
mkdir -p ${BACKUP_DIR}

# create a backup
curl -X POST \
    -F "master_pwd=${ADMIN_PASSWORD}" \
    -F "name=${ODOO_DATABASE}" \
    -F "backup_format=zip" \
    -o ${BACKUP_DIR}/${ODOO_DATABASE}.$(date +%F).zip \
    http://localhost:8069/web/database/backup

#keep 31 file
for i in `cd ${BACKUP_DIR} && ls -1tr | head -n -31`;do
	aws s3 rm s3://usasiahours-backup-logs/database_backups/daily_backup/$i
done
cd ${BACKUP_DIR} && ls -1tr | head -n -31 | xargs -d '\n' rm -f --
