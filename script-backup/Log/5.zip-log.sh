#!/bin/bash

datefile=`date +%Y.%m.%d -d '-1 day'`
datefolder=`date +%Y.%m -d '-1 day'`
year=`date +%Y -d '-1 day'`
#zip file
cd /opt/backup/log_backups/$year/$datefolder
/usr/bin/zip poemsquare-prod-$datefile.json.zip poemsquare-prod-$datefile.json
/usr/bin/rm -rf poemsquare-prod-$datefile.json

