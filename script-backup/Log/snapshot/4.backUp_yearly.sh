#!/bin/bash

datefile=`date +%Y%m%d -d '-1 day'`
date=`date +%Y -d '-1 day'`
BACKUP_DIR=/opt/backup/snapshot/year_backup

mkdir -p $BACKUP_DIR/$datefile
cd /opt/poem-core/snapshot
ls -l | grep "$date" | cp -f * $BACKUP_DIR/$datefile

cd $BACKUP_DIR
zip -r snapshot_$datefile.zip $datefile
rm -rf $datefile

aws s3 cp $BACKUP_DIR/snapshot_$datefile.zip s3://usasiahours-noncore/snapshot_backup/year_backup

for i in `cd ${BACKUP_DIR} && ls -1tr | head -n -10`;do
        aws s3 rm s3://usasiahours-noncore/snapshot_backup/year_backup/$i
done

#cd ${BACKUP_DIR} && ls -1tr | head -n -10 | xargs -d '\n' rm -f --