#!/bin/bash

datefile=`date +%Y%m%d -d '-1 day'`
date=`date +%Y -d '-1 day'`
BACKUP_DIR=/opt/backup/history/year_backup

mkdir -p $BACKUP_DIR/$datefile
cd /opt/poem-core/history
ls -l | grep "$date"| cp -f * $BACKUP_DIR/$datefile

cd $BACKUP_DIR
zip -r history_$datefile.zip $datefile
rm -rf $datefile

aws s3 cp $BACKUP_DIR/history_$datefile.zip s3://usasiahours-noncore/history_backup/year_backup

for i in `cd ${BACKUP_DIR} && ls -1tr | head -n -10`;do
        aws s3 rm s3://usasiahours-noncore/history_backup/year_backup/$i
done

#cd ${BACKUP_DIR} && ls -1tr | head -n -10 | xargs -d '\n' rm -f --