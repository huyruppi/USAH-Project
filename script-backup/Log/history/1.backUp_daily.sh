#!/bin/bash

datefile=`date +%Y%m%d -d '-1 day'`
date=`date +%b' '%d -d '-1 day'`
BACKUP_DIR=/opt/backup/history/daily_backup

mkdir -p $BACKUP_DIR/$datefile
cd /opt/poem-core/history
ls -l | grep "$date" | cp -f * $BACKUP_DIR/$datefile

cd $BACKUP_DIR
zip -r history_$datefile.zip $datefile
rm -rf $datefile

aws s3 cp $BACKUP_DIR/history_$datefile.zip s3://usasiahours-noncore/history_backup/daily_backup

for i in `cd ${BACKUP_DIR} && ls -1tr | head -n -31`;do
        aws s3 rm s3://usasiahours-noncore/history_backup/daily_backup/$i
done