#!/bin/bash
# Check S3
Year=`date +%Y`
Month=`date +%Y.%m`
s3_logs=`aws s3 ls s3://usasiahours-backup-logs/log_backups/${Year}/${Month}/`
local_logs=`cd /opt/backup/log_backups/ && ls -l ${Year}/${Month}/ | awk -F' ' '{print $5" "$7" "$8" "$9}'`
db=("daily_backup" "week_backup" "month_backup" "year_backup")
echo "Subject: Check All Backup.
From: OMTDT@usasiahrs.poem2.com"
echo "`date`"
echo "Logs Backup list!!"
echo -e "\nLogs on S3\n"
echo -e "$s3_logs"

echo -e "\nLogs on Server"
echo -e "$local_logs"
echo -e "---------------------------"


echo -e "\nDatabase Backup list!!"
for i in ${db[@]}; do
	echo -e "\n${i} DB backup list on S3\n"
	s3_db=`aws s3 ls s3://usasiahours-backup-logs/database_backups/${i}/`
	echo -e "$s3_db"
done


for i in ${db[@]}; do
	echo -e "\n${i} DB backup list on Server"
	local_db=`cd /opt/backup/database_backups/${i} && ls -l | awk -F' ' '{print $5" "$6"-"$7" "$8" "$9}'`
	echo -e "$local_db"
done
echo -e "\nKindly be informed!"
