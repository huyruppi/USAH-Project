Subject: Check All Backup.
From: OMTDT@usasiahrs.poem2.com
Tue Sep 20 06:45:41 +08 2022
Logs Backup list!!

Logs on S3

2022-09-02 06:45:26  171105604 poemsquare-prod-2022.09.01.json.zip
2022-09-03 06:45:28  128449025 poemsquare-prod-2022.09.02.json.zip
2022-09-04 06:45:55   31279191 poemsquare-prod-2022.09.03.json.zip
2022-09-05 06:45:31   17752383 poemsquare-prod-2022.09.04.json.zip
2022-09-06 06:45:30  130828887 poemsquare-prod-2022.09.05.json.zip
2022-09-07 06:45:31  131623843 poemsquare-prod-2022.09.06.json.zip
2022-09-08 06:45:31  149800425 poemsquare-prod-2022.09.07.json.zip
2022-09-09 06:45:31  167786342 poemsquare-prod-2022.09.08.json.zip
2022-09-10 06:45:32  143358415 poemsquare-prod-2022.09.09.json.zip
2022-09-11 06:45:33   37179592 poemsquare-prod-2022.09.10.json.zip
2022-09-12 06:45:33   19480304 poemsquare-prod-2022.09.11.json.zip
2022-09-13 06:45:34  169275366 poemsquare-prod-2022.09.12.json.zip
2022-09-14 06:45:35  221536523 poemsquare-prod-2022.09.13.json.zip
2022-09-15 06:45:36  154520010 poemsquare-prod-2022.09.14.json.zip
2022-09-16 06:45:36  169783285 poemsquare-prod-2022.09.15.json.zip
2022-09-17 06:45:37  148896597 poemsquare-prod-2022.09.16.json.zip
2022-09-18 06:45:38   16966920 poemsquare-prod-2022.09.17.json.zip
2022-09-19 06:45:39   15717051 poemsquare-prod-2022.09.18.json.zip
2022-09-20 06:45:41  150830361 poemsquare-prod-2022.09.19.json.zip

Logs on Server
   
171105604 2 02:00 poemsquare-prod-2022.09.01.json.zip
128449025 3 02:00 poemsquare-prod-2022.09.02.json.zip
31279191 4 02:00 poemsquare-prod-2022.09.03.json.zip
17752383 5 02:00 poemsquare-prod-2022.09.04.json.zip
130828887 6 02:00 poemsquare-prod-2022.09.05.json.zip
131623843 7 02:00 poemsquare-prod-2022.09.06.json.zip
149800425 8 02:00 poemsquare-prod-2022.09.07.json.zip
167786342 9 02:00 poemsquare-prod-2022.09.08.json.zip
143358415 10 02:00 poemsquare-prod-2022.09.09.json.zip
37179592 11 02:00 poemsquare-prod-2022.09.10.json.zip
19480304 12 02:00 poemsquare-prod-2022.09.11.json.zip
169275366 13 02:00 poemsquare-prod-2022.09.12.json.zip
221536523 14 02:00 poemsquare-prod-2022.09.13.json.zip
154520010 15 02:00 poemsquare-prod-2022.09.14.json.zip
169783285 16 02:00 poemsquare-prod-2022.09.15.json.zip
148896597 17 02:00 poemsquare-prod-2022.09.16.json.zip
16966920 18 02:00 poemsquare-prod-2022.09.17.json.zip
15717051 19 02:00 poemsquare-prod-2022.09.18.json.zip
150830361 20 02:00 poemsquare-prod-2022.09.19.json.zip
---------------------------

Database Backup list!!

daily_backup DB backup list on S3

2022-08-21 06:45:21   54997838 poemsquare_prod.2022-08-21.zip
2022-08-22 06:45:19   55925181 poemsquare_prod.2022-08-22.zip
2022-08-23 06:45:20   57339143 poemsquare_prod.2022-08-23.zip
2022-08-24 06:45:21   59319615 poemsquare_prod.2022-08-24.zip
2022-08-25 06:45:22   60879315 poemsquare_prod.2022-08-25.zip
2022-08-26 06:45:22   62651810 poemsquare_prod.2022-08-26.zip
2022-08-27 06:45:22   64421989 poemsquare_prod.2022-08-27.zip
2022-08-28 06:45:24   65975411 poemsquare_prod.2022-08-28.zip
2022-08-29 06:45:25   66003209 poemsquare_prod.2022-08-29.zip
2022-08-30 06:45:24   67454720 poemsquare_prod.2022-08-30.zip
2022-08-31 06:45:26   69293396 poemsquare_prod.2022-08-31.zip
2022-09-01 06:45:26   70335835 poemsquare_prod.2022-09-01.zip
2022-09-02 06:45:26   71343523 poemsquare_prod.2022-09-02.zip
2022-09-03 06:45:27   72460737 poemsquare_prod.2022-09-03.zip
2022-09-04 06:45:28   72498850 poemsquare_prod.2022-09-04.zip
2022-09-05 06:45:31   72524997 poemsquare_prod.2022-09-05.zip
2022-09-06 06:45:29   74660681 poemsquare_prod.2022-09-06.zip
2022-09-07 06:45:30   75864274 poemsquare_prod.2022-09-07.zip
2022-09-08 06:45:31   76792432 poemsquare_prod.2022-09-08.zip
2022-09-09 06:45:31   78546235 poemsquare_prod.2022-09-09.zip
2022-09-10 06:45:32   80060099 poemsquare_prod.2022-09-10.zip
2022-09-11 06:45:32   80105762 poemsquare_prod.2022-09-11.zip
2022-09-12 06:45:32   80138320 poemsquare_prod.2022-09-12.zip
2022-09-13 06:45:33   81204825 poemsquare_prod.2022-09-13.zip
2022-09-14 06:45:35   83013606 poemsquare_prod.2022-09-14.zip
2022-09-15 06:45:35   84271574 poemsquare_prod.2022-09-15.zip
2022-09-16 06:45:36   85212873 poemsquare_prod.2022-09-16.zip
2022-09-17 06:45:36   86372424 poemsquare_prod.2022-09-17.zip
2022-09-18 06:45:37   86413525 poemsquare_prod.2022-09-18.zip
2022-09-19 06:45:38   86457495 poemsquare_prod.2022-09-19.zip
2022-09-20 06:45:40   87599738 poemsquare_prod.2022-09-20.zip
2022-08-21 06:45:21   54888377 poemsquare_prod.20220820_1031.zip

week_backup DB backup list on S3

2022-07-17 06:45:03    3918803 poemsquare_prod.2022-07-17.zip
2022-07-24 06:45:03    6958709 poemsquare_prod.2022-07-24.zip
2022-07-31 06:45:06   16966437 poemsquare_prod.2022-07-31.zip
2022-08-07 06:45:09   27571464 poemsquare_prod.2022-08-07.zip
2022-08-14 06:45:15   41918617 poemsquare_prod.2022-08-14.zip
2022-08-21 06:45:21   54997838 poemsquare_prod.2022-08-21.zip
2022-08-28 06:45:26   65975411 poemsquare_prod.2022-08-28.zip
2022-09-04 06:45:29   72498850 poemsquare_prod.2022-09-04.zip
2022-09-11 06:45:32   80105762 poemsquare_prod.2022-09-11.zip
2022-09-18 06:45:37   86413525 poemsquare_prod.2022-09-18.zip

month_backup DB backup list on S3

2022-08-01 06:45:06   17004090 poemsquare_prod.2022-08-01.zip
2022-09-01 06:45:26   70335835 poemsquare_prod.2022-09-01.zip

year_backup DB backup list on S3



daily_backup DB backup list on Server
 -  
54888377 Aug-20 11:32 poemsquare_prod.20220820_1031.zip
54997838 Aug-21 00:00 poemsquare_prod.2022-08-21.zip
55925181 Aug-22 00:00 poemsquare_prod.2022-08-22.zip
57339143 Aug-23 00:00 poemsquare_prod.2022-08-23.zip
59319615 Aug-24 00:00 poemsquare_prod.2022-08-24.zip
60879315 Aug-25 00:00 poemsquare_prod.2022-08-25.zip
62651810 Aug-26 00:00 poemsquare_prod.2022-08-26.zip
65975411 Aug-28 00:00 poemsquare_prod.2022-08-28.zip
66003209 Aug-29 00:00 poemsquare_prod.2022-08-29.zip
67454720 Aug-30 00:00 poemsquare_prod.2022-08-30.zip
69293396 Aug-31 00:00 poemsquare_prod.2022-08-31.zip
70335835 Sep-1 00:00 poemsquare_prod.2022-09-01.zip
71343523 Sep-2 00:00 poemsquare_prod.2022-09-02.zip
72460737 Sep-3 00:00 poemsquare_prod.2022-09-03.zip
72498850 Sep-4 00:00 poemsquare_prod.2022-09-04.zip
72524997 Sep-5 00:00 poemsquare_prod.2022-09-05.zip
74660681 Sep-6 00:00 poemsquare_prod.2022-09-06.zip
75864274 Sep-7 00:00 poemsquare_prod.2022-09-07.zip
76792432 Sep-8 00:00 poemsquare_prod.2022-09-08.zip
78546235 Sep-9 00:00 poemsquare_prod.2022-09-09.zip
80060099 Sep-10 00:00 poemsquare_prod.2022-09-10.zip
80105762 Sep-11 00:00 poemsquare_prod.2022-09-11.zip
80138320 Sep-12 00:00 poemsquare_prod.2022-09-12.zip
81204825 Sep-13 00:00 poemsquare_prod.2022-09-13.zip
83013606 Sep-14 00:00 poemsquare_prod.2022-09-14.zip
84271574 Sep-15 00:00 poemsquare_prod.2022-09-15.zip
85212873 Sep-16 00:00 poemsquare_prod.2022-09-16.zip
86372424 Sep-17 00:00 poemsquare_prod.2022-09-17.zip
86413525 Sep-18 00:00 poemsquare_prod.2022-09-18.zip
86457495 Sep-19 00:00 poemsquare_prod.2022-09-19.zip
87599738 Sep-20 00:00 poemsquare_prod.2022-09-20.zip

week_backup DB backup list on Server
 -  
3918803 Jul-17 01:00 poemsquare_prod.2022-07-17.zip
6958709 Jul-24 01:00 poemsquare_prod.2022-07-24.zip
16966437 Jul-31 01:00 poemsquare_prod.2022-07-31.zip
27571464 Aug-7 01:00 poemsquare_prod.2022-08-07.zip
41918617 Aug-14 01:00 poemsquare_prod.2022-08-14.zip
54997838 Aug-21 01:00 poemsquare_prod.2022-08-21.zip
65975411 Aug-28 01:00 poemsquare_prod.2022-08-28.zip
72498850 Sep-4 01:00 poemsquare_prod.2022-09-04.zip
80105762 Sep-11 01:00 poemsquare_prod.2022-09-11.zip
86413525 Sep-18 01:00 poemsquare_prod.2022-09-18.zip

month_backup DB backup list on Server
 -  
17004090 Aug-1 01:00 poemsquare_prod.2022-08-01.zip
70335835 Sep-1 01:00 poemsquare_prod.2022-09-01.zip

year_backup DB backup list on Server
 -  

Kindly be informed!
