#!/bin/bash
# Report All System
echo "Subject: All status of Server and Service PROD Environment.
From: OMTDT@usasiahrs.poem2.com"
echo "`date`"
echo -e "\nDear all, \nOM team report all status of Server and Service PROD Environment."
echo -e "\n---------------------REPORT STATUS ALL Server---------------------"
ipa=("10.148.72.99" "10.148.73.99")
ipw=( "10.148.72.68" "10.148.73.68")
container=("poem-lp" "poem-retail" "poem-ws" "poem-authserver" "poem-gateway" "poem-marketservice" "poem-registry")
command='docker ps --format "- CID_{{.Names}}: {{.ID}} --- Time: {{.Status}}"'
#Check Server
         for ip in ${ipa[@]}
                do
                        host_name=`ssh poem@${ip} hostname`
                        if [ "ip-10-148-72-99.ap-southeast-1.compute.internal" == $host_name ]; then
                                hostname="PROD_BACKEND:"
                        else
                                hostname="PROD_CORE:"
                        fi
                        #DISK_USE=`ssh poem@${ip} df -h / | grep -vi 'avail|mapper' | grep dev | awk '{print $5}'`
                        DISK_USE=`ssh poem@${ip} df -h / | grep dev | awk '{print $5}'`
                        MEMORY=`ssh poem@${ip} free -m | awk 'NR==2{printf "%.2f%%\t\t", $3*100/$2 }'`
                        CPU=`ssh poem@${ip} cat /proc/loadavg | awk '{print $1, $2, $3}'`
                        echo -e "- "$hostname"\t D: "$DISK_USE"\t M: "$MEMORY"\t C: "$CPU""
        done
        for ip in ${ipw[@]}
                do
                        host_name_work=`ssh poem@${ip} hostname`
                        if [ "ip-10-148-72-68.ap-southeast-1.compute.internal" == $host_name_work ]; then
                                hostname="PROD_WORKER_A:"
                        else
                                hostname="PROD_WORKER_B:"
                        fi
                        DISK_USE=`ssh poem@${ip} df -h / | grep dev | awk '{print $5}'`
                        MEMORY=`ssh poem@${ip} free -m | awk 'NR==2{printf "%.2f%%\t\t", $3*100/$2 }'`
                        CPU=`ssh poem@${ip} cat /proc/loadavg | awk '{print $1, $2, $3}'`
                        echo -e "- "$hostname" D: "$DISK_USE"\t M: "$MEMORY"\t C: "$CPU""
        done
# Check PID, PID_NAME Service
        echo -e "\n-----------------REPORT STATUS ALL Service------------------"

        maintrade=`ssh poem@10.148.73.99 pgrep -f matching-gw-0.0.1.jar`
        grok_time=`ssh poem@10.148.73.99 systemctl status poem-core | grep "Active" | awk -F';' '{print $2}'`
        echo -e "- "PID_Core:"\t "$maintrade" ----------UPTIME: $grok_time"

        pid_batch=`pgrep -f batch-0.0.1-SNAPSHOT.jar`
        time_batch=`systemctl status poem-batch | grep "Active" | awk -F';' '{print $2}'`
        echo -e "- "PID_Batch:"\t "$pid_batch" -----------UPTIME: $time_batch"

#Check ELK service
        docker ps --format "- CID_{{.Names}}: {{.ID}} ---- Time: {{.Status}}"

#Check Container ID Worker-1
        echo -e "\n--------------REPORT STATUS ALL Container Worker-A------------------"
        ssh poem@10.148.72.68 $command 
#Check Container ID Worker-2
        echo -e "\n--------------REPORT STATUS ALL Container Worker-B------------------"
        ssh poem@10.148.73.68 $command
echo -e "\n------------------------ALL REPORT SYSTEM: DONE-----------------------"
echo -e "\nKindly be informed!"

